// CCI Application
// Last Update: Feburary 6, 2018

// Require request, request promise, and promise libraries
var requestPromise = require('request-promise').defaults({ resolveWithFullResponse: true });
var promise = require('promise');
var CCI = function(){};

// Read the logs from "run command template" and get only the last 50 lines of logs
// Input: preLogs are the complete logs from runCommandTemplate output
// Return: payloadNew is the output of runCommandTemplate but the response contain only 50 lines for the logs
CCI.prototype.parseLogs = function(preLogs,callback){
    
    var payloadNew = preLogs;
    var cResults=preLogs["commands_results"]; // Get all the results for all the devices

    // For each device results get the response that came back from the MOP
    var res="";
    for(var i=0;i<cResults.length;i++){
        res=cResults[i].response;

        // Remove the extra stuff from the response
        res=res.substring(res.indexOf('Log Buffer ('));
        
        // Split the response lines
        var postLogs=res.split('\r\n');

        // For each line add new line tags
        for(var j=0; j<postLogs.length; j++){
            postLogs[j]= postLogs[j]+ "\r\n";
        }

        // If there are more than 50 lines in the log, return only the last 50
        if(postLogs.length>=50){
            postLogs=postLogs.slice(-50);
        }
        
        // Reconstruct the same MOP response but with the shorter lines, and remove the comma from it
        payloadNew["commands_results"][i].response = postLogs.toString().replace(/,/g," ");
    }
   
    // Return the new runCommandTemplate output
    return callback(payloadNew);
}

// Send a POST notification to a server
// Input:serverUrl is the url where to send the notification to
// Input: taskName is the name of the task from the workflow
// Input: jobId the job's id from the workflow
// Input: customMsg any custom message you want, usually represent the status "success" or "failed"
// Input: attachmentMsg anything you would like to attach to the message
// Return: body which is the message that was sent or error if it did not send
CCI.prototype.notifyServer = function(serverURL, taskName, jobId, customMsg, attachmentMsg, callback){

    // This is the message body that will be sent
    var requestData = {
        "objMsg":{
            "job_id": jobId,
            "task": taskName,
            "msg": customMsg,
            "attachedBody": attachmentMsg
        }
    }

    // Just a simple promise request to send POST message to the serverUrl along with the rest of the information
    requestPromise({
        url: serverURL,
        method: "POST",
        json: requestData
    }).then(function(body){
        callback(body);
    }).catch(function(err){
      callback(err);  
    });   
}

// Split the sites in L2VPN for the workflow to go over each site by its own
// Input: completePayload this is the job instance with all the information
// Return: splittedPayLoad this is an array of sites, each item represent a site
CCI.prototype.splitL2VPN = function(completePayload, callback){

    // Remove all the extra information and take only the site array
    var siteArray = completePayload['/mef-services:l2vpn'][0].site;
    
    var splittedPayload = [];
    
    // Loop on the array of site, and for each item put it in an array
    for(var i=0; i<siteArray.length;i++){
      var site = siteArray[i];
      splittedPayload[i]=site;
    }
    
    callback(splittedPayload);
}

// Validate the design of payload by passing it to workflow up to the commit devices
// Input: data this is the payload that is coming from the POST request this should contain an object instance and a string service(UNI, DIA, or L2VPN)
// Return: results this is an object containing the status of validation whether it is correct or not, and any errors found
CCI.prototype.validateDesign = function(data, callback){
   
   // Variables definitions
    var timer1="";
    var jobId="";
    var jobStatus="";
    var results={
        "status":"",
        "msg":"",
        "jobid": ""
    };
    var jobError="";
    var workflowName="";

    // Check if the data that is passed does not contain instance, does not contain service, or any of them are empty
    if(!data.hasOwnProperty("instance") || data.instance.length<=3 || !data.hasOwnProperty("device_turnup") || data.device_turnup.length<=0 || !data["device_turnup"][0]){
        results.status="Failed";
        results.msg="Please fill out all the required information, instance (Object), and device_turnup (Array)."
        callback(results);
    }else{
    
    // Based on the service from the POST request, decide which workflow to start
    if(data["instance"]["/mef-services:uni"]){
        workflowName="UNI-Activate-ValidateDesign";
    }else if(data["instance"]["/mef-services:dia"]){
        workflowName="DIA-Activate-ValidateDesign";
    }else if(data["instance"]["/mef-services:l2vpn"]){
        workflowName="L2VPN-Activate-ValidateDesign";
    }else{
        results.status="Failed";
        results.msg="Service name is incorrect";
        callback(results);
    }
    
    // Start the workflow with the data from the POST request
    var startJobPromise = new promise(function(resolve, reject){
        cogs.WorkFlowEngine.startJob(workflowName,"Automated Design Validation Running " + workflowName, data, (job, serror) => {
            if(serror){
                reject(serror);
            }
            jobId=job._id;
            jobStatus=job.status;
            resolve();
        });
    });

    // If the job started continue
    startJobPromise.then(function(){
        
        // Check the status of a giving job and update jobStatus
        function checkStatusFun(){
            // If the jobStatus is not "running" then send something back
            if(jobStatus!="running"){
                clearInterval(timer1);
                
                if(jobStatus==="canceled"){ // If cancelled, send cancelled, this is mostly due manual cancellation
                    console.log("Design Cancelled :(")
                    results.status="Cancelled";
                    results.msg="Job was cancelled";
                    callback(results);
                }else if(jobStatus==="error"){ // If error, send error with the error, and cancel the job
                    console.log("Design Errored :(")
                    results.status="Failed";
                    results.jobid = jobId;
                    results.msg = jobError;
                    cogs.WorkFlowEngine.cancelJob(jobId, (jobCancelled, serror) => {
                    callback(results);
                    })
                }else{ // If not cancel, error, or running then send success
                    console.log("Design validated :D")
                    results.status="Success";
                    results.msg="Design validated and all good";
                    callback(results);
                }
               
            }
            // Go to workflow engine and check the job status there
            var checkJobStatusPromise = new promise(function(resolve,reject){
                cogs.WorkFlowEngine.getJob(jobId, (jobs, serror) => {
                    if(serror){
                        reject(serror)
                    }
                    jobError=jobs.error;
                    resolve(jobs.status);
                })
            })

            checkJobStatusPromise.then(function(fromResolve){
                jobStatus=fromResolve;
                console.log("I am still running and checking the job status for validation. :D");
            }).catch(function(fromReject){
                console.log(":(")
                results.status="Failed";
                results.msg="Could not check job status." + fromReject;
                callback(results);
            })
        }

        // Keep running check job status every 5 seconds until the status "not running"
        timer1= setInterval(checkStatusFun,5000);

        // Return an error if the workflow did not start either because it is missing information in instance, or sent the wrong variables to it
    }).catch(function(fromReject){
        results.status = "Failed";
        results.msg = "Could not start workflow." + fromReject;
        callback(results);
    });
    }
}

// Build a service path for a service Circuit ID
// Input: serviceModelPath the name of the service model
// Input: circuitId the service Circuit ID
// Output: servicePath is the full path for the service Circuit ID
CCI.prototype.buildServicePath = function(serviceModelPath, circuitId, callback){
    // Build the service path and return it
    var servicePath=[];
    servicePath[0] = serviceModelPath + "{" + circuitId + "}";
    callback(servicePath);
}

// Check two instances payload to decide if need to rerun precheck
// Input: serviceModel the name of the service model (uni,dia,l2vnp)
// Input: oldInstance the payload of the provisioned instance
// Input: newInstance the payload of the new instance
// Output: results the results of need to precheck for the new instance
CCI.prototype.needPrecheck = function(serviceModel, oldInstance, newInstance, callback){
    
    // Return either true or false to know if precheck needed or not
    var results = "";

    if(serviceModel==="uni"){ // UNI - Check interface changes
        if(oldInstance["/mef-services:uni"][0]["device-name"] != newInstance["/mef-services:uni"][0]["device-name"] 
        || oldInstance["/mef-services:uni"][0]["interface-type"] != newInstance["/mef-services:uni"][0]["interface-type"]
        || oldInstance["/mef-services:uni"][0]["interface-id"] != newInstance["/mef-services:uni"][0]["interface-id"]
        ){
            results="true";
            return callback(results);
        }else{
            results="false";
            return callback(results);
        }
        
    }else if(serviceModel==="dia-encap"){ // DIA - Check encapsulation (dot1q, seconddot1q) changes

        if(oldInstance["/mef-services:dia"][0]["device-name"] != newInstance["/mef-services:dia"][0]["device-name"] 
        || oldInstance["/mef-services:dia"][0]["pe-interface-details"]["interface-type"] != newInstance["/mef-services:dia"][0]["pe-interface-details"][0]["interface-type"]
        || oldInstance["/mef-services:dia"][0]["pe-interface-details"]["interface-id"] != newInstance["/mef-services:dia"][0]["pe-interface-details"][0]["interface-id"]
        || oldInstance["/mef-services:dia"][0]["pe-interface-details"]["service-id-number"] != newInstance["/mef-services:dia"][0]["pe-interface-details"][0]["service-id-number"]){
            results="true";
            return callback(results);
        }else{
            if(newInstance["/mef-services:dia"][0]["design-type"] === "DarkFiber"){
                if(newInstance["/mef-services:dia"][0]["encapsulation-type"][0]["dot1q-Id"] != oldInstance["/mef-services:dia"][0]["encapsulation-type"]["dot1q-Id"]){
                    return callback("true");
                }else{
                    return callback("false");
                }
            }else{
                if(newInstance["/mef-services:dia"][0]["encapsulation-type"][0]["dot1q-Id"] != oldInstance["/mef-services:dia"][0]["encapsulation-type"]["dot1q-Id"]){
                    return callback("true");
                }else{
                    if(newInstance["/mef-services:dia"][0]["encapsulation-type"][0]["seconddot1q-Id"] != oldInstance["/mef-services:dia"][0]["encapsulation-type"]["seconddot1q-Id"]){
                        return callback("true");
                    }else{
                        if(newInstance["/mef-services:dia"][0]["encapsulation-type"][0]["dot1ad"] != oldInstance["/mef-services:dia"][0]["encapsulation-type"]["dot1ad"]){
                            return callback("true");
                        }else{
                            return callback("false");
                        }
                    }
                }
            }
        }
    }else if(serviceModel==="dia-ip"){ // DIA - Check IP address (mnain IP address only) changes
        var results="";

        if(oldInstance["/mef-services:dia"][0]["device-name"] != newInstance["/mef-services:dia"][0]["device-name"]
        || oldInstance["/mef-services:dia"][0]["address-info"]["ipaddress"] != newInstance["/mef-services:dia"][0]["address-info"][0]["ipaddress"]
        || oldInstance["/mef-services:dia"][0]["address-info"]["mask"] != newInstance["/mef-services:dia"][0]["address-info"][0]["mask"]){
            results="true";
            return callback(results);
        }else{
            results="false";
            return callback(results);
        }
    
    }else if(serviceModel==="dia-subinterface"){ // DIA - Check subinterface changes
      console.log(oldInstance["/mef-services:dia"][0]["device-name"]);
      console.log(oldInstance["/mef-services:dia"][0]["pe-interface-details"]["interface-type"]);
      console.log(oldInstance["/mef-services:dia"][0]["pe-interface-details"]["interface-id"]);
      console.log(oldInstance["/mef-services:dia"][0]["pe-interface-details"]["service-id-number"]);

      console.log(newInstance["/mef-services:dia"][0]["device-name"]);
      console.log(newInstance["/mef-services:dia"][0]["pe-interface-details"][0]["interface-type"]);
      console.log(newInstance["/mef-services:dia"][0]["pe-interface-details"][0]["interface-id"]);
      console.log(newInstance["/mef-services:dia"][0]["pe-interface-details"][0]["service-id-number"]);

        if(oldInstance["/mef-services:dia"][0]["device-name"] != newInstance["/mef-services:dia"][0]["device-name"] 
        || oldInstance["/mef-services:dia"][0]["pe-interface-details"]["interface-type"] != newInstance["/mef-services:dia"][0]["pe-interface-details"][0]["interface-type"]
        || oldInstance["/mef-services:dia"][0]["pe-interface-details"]["interface-id"] != newInstance["/mef-services:dia"][0]["pe-interface-details"][0]["interface-id"]
        || oldInstance["/mef-services:dia"][0]["pe-interface-details"]["service-id-number"] != newInstance["/mef-services:dia"][0]["pe-interface-details"][0]["service-id-number"]){
            results="true";
            return callback(results);
        }else{
            results="false";
            return callback(results);
        }
    
    }else if(serviceModel==="dia-ip2nd"){ // DIA - Check secondary IP addresses
        console.log("dia-ip2nd");
        if(oldInstance["/mef-services:dia"][0]["device-name"] != newInstance["/mef-services:dia"][0]["device-name"]){
            console.log("device changed");
            results="true";
            return callback(newInstance["/mef-services:dia"][0]["address-info"][0]["secondary-ipaddress-info"]);
        }else{
            console.log("device did not change");
            if(!oldInstance["/mef-services:dia"][0]["address-info"]["pfx-ln-secondary"]){
                console.log("old instance does not have secondary ip");
                return callback(newInstance["/mef-services:dia"][0]["address-info"][0]["secondary-ipaddress-info"]);
            }else{
                console.log("old instance secondary ip is set to false");
                if(oldInstance["/mef-services:dia"][0]["address-info"]["pfx-ln-secondary"] === "false"){
                    return callback(newInstance["/mef-services:dia"][0]["address-info"][0]["secondary-ipaddress-info"]);
                }
            }

            var oldArray=[];

            if(oldInstance["/mef-services:dia"][0]["address-info"]["secondary-ipaddress-info"][0]){
                console.log("Oldinstance have array of ip addresses");
                oldArray = oldInstance["/mef-services:dia"][0]["address-info"]["secondary-ipaddress-info"];
            }else{
                console.log("Oldinstance have one ip address so I will push it to an array");
                oldArray.push(oldInstance["/mef-services:dia"][0]["address-info"]["secondary-ipaddress-info"]);
            }

            // var newArray = newInstance["/mef-services:dia"][0]["address-info"][0]["secondary-ipaddress-info"];
            var newArray=[];
            
            if(newInstance["/mef-services:dia"][0]["address-info"][0]["secondary-ipaddress-info"][0]){
                console.log("newinstance ip addresses are array");
                newArray = newInstance["/mef-services:dia"][0]["address-info"][0]["secondary-ipaddress-info"];
            }else{
                console.log("newinstance ip address I will push it to an array");
                newArray.push(newInstance["/mef-services:dia"][0]["address-info"]["secondary-ipaddress-info"]);
            }

            var arrayToCheck = [];

            var changed="false";

            var count=0;
            var count2=0;

            console.log("newarray length"+newArray.length);
            console.log("oldarray length"+oldArray.length);

            for(i=0;i<newArray.length;i++){
                count=0;
                for(j=0;j<oldArray.length;j++){
                    if(newArray[i]["secondary-ipaddress"]===oldArray[j]["secondary-ipaddress"] && newArray[i]["secondary-mask"]===oldArray[j]["secondary-mask"]){
                    count++;
                    }
                }
                if(count<1){
                    arrayToCheck[count2]=newArray[i];
                    count2++;
                }
            }

            if (arrayToCheck.length>=1){
                return callback(arrayToCheck);
            }else{
                results="false";
                return callback(results);
            }
        }

    }else if(serviceModel==="dia-ipstatic"){ // DIA - Check IP static addresses (forwarding addresses)

        if(oldInstance["/mef-services:dia"][0]["device-name"] != newInstance["/mef-services:dia"][0]["device-name"]){
            results="true";
            console.log("here");
            return callback(newInstance["/mef-services:dia"][0]["protocol-static"][0]);
        }else{

            if(!oldInstance["/mef-services:dia"][0]["setup-protocol-static"]){
                return callback(newInstance["/mef-services:dia"][0]["protocol-static"][0]);
            }else{
                if (oldInstance["/mef-services:dia"][0]["setup-protocol-static"]==="false"){
                    return callback(newInstance["/mef-services:dia"][0]["protocol-static"][0]);
                }
            }

            var oldArray=[];

            if(oldInstance["/mef-services:dia"][0]["protocol-static"][0]){
                console.log("old instance dia array");
                oldArray = oldInstance["/mef-services:dia"][0]["protocol-static"];
            }else{
                console.log("old instance dia non array");
                oldArray.push(oldInstance["/mef-services:dia"][0]["protocol-static"]);
            }

            console.log("oldArray "+JSON.stringify(oldArray));

            var newArray = "";

            if(newInstance["/mef-services:dia"][0]["protocol-static"][0]){
                console.log("new instance dia array");
                newArray = newInstance["/mef-services:dia"][0]["protocol-static"];
            }else{
                console.log("new instance dia non array");
                newArray.push(newInstance["/mef-services:dia"][0]["protocol-static"]);
            }

            console.log("newArray "+JSON.stringify(oldArray));

            console.log("here");

            var arrayToCheck = [];

            var changed="false";

            var count=0;
            var count2=0;

            for(i=0;i<newArray.length;i++){
                console.log("inside the first for loop for newArray")
                count=0;
                for(j=0;j<oldArray.length;j++){
                    console.log("here");
                    console.log(newArray[i]["destination-prefix"]);
                    console.log(oldArray[j]["destination-prefix"]);
                    if(newArray[i]["destination-prefix"]===oldArray[j]["destination-prefix"]){
                        console.log("here");

                    count++;
                    }
                }
                if(count<1){
                    arrayToCheck[count2]=newArray[i];
                    count2++;
                }
            }

            if (arrayToCheck.length>=1){
                return callback(arrayToCheck);
            }else{
                results="false";
                return callback(results);
            }
        }

    }else if(serviceModel==="dia-cpe"){ // Check CPE on DIA (consume-existing-uni=no) changes
        var payloadsSimilar = false;
        var results= "";
        
        if(oldInstance["setup-uni-contents"]){
            payloadsSimilar = true;
        }

        if(payloadsSimilar==true){

            if(oldInstance["/mef-services:dia"][0]["setup-uni-contents"]["circuit-id"] != newInstance["/mef-services:dia"][0]["setup-uni-contents"][0]["circuit-id"]){
                results="true";
                console.log(results);
                return callback(results);
            }else{
                if(oldInstance["/mef-services:dia"][0]["setup-uni-contents"]["device-name"] != newInstance["/mef-services:dia"][0]["setup-uni-contents"][0]["device-name"] 
                || oldInstance["/mef-services:dia"][0]["setup-uni-contents"]["interface-type"] != newInstance["/mef-services:dia"][0]["setup-uni-contents"][0]["interface-type"]
                || oldInstance["/mef-services:dia"][0]["setup-uni-contents"]["interface-id"] != newInstance["/mef-services:dia"][0]["setup-uni-contents"][0]["interface-id"]
                ){
                    results="true";
                    console.log(results);
                    return callback(results);
                }else{
                    results="false";
                    console.log(results);
                    return callback(results);
                }
            }
        }else{
            if(oldInstance["/mef-services:dia"][0]["uni"] != newInstance["/mef-services:dia"][0]["setup-uni-contents"][0]["circuit-id"]){
                results="true";
                console.log(results);
                return callback(results);
            }else{
                // if(oldInstance["/mef-services:dia"][0]["device-name"] != newInstance["/mef-services:dia"][0]["setup-uni-contents"][0]["device-name"] 
                // || oldInstance["/mef-services:dia"][0]["interface-type"] != newInstance["/mef-services:dia"][0]["setup-uni-contents"][0]["interface-type"]
                // || oldInstance["/mef-services:dia"][0]["interface-id"] != newInstance["/mef-services:dia"][0]["setup-uni-contents"][0]["interface-id"]
                // ){
                //     results="true";
                //     console.log(results);
                //     return callback(results);
                // }else{
                //     results="false";
                //     console.log(results);
                //     return callback(results);
                // }

                results="false";
                console.log(results);
                return callback(results);
            }
        }      
    }else if(serviceModel==="l2vpn-encap"){ // Check L2VPN encapsulation (dot1q, seconddot1q, dot1ad) changes
        var oldSites=[];

        if(oldInstance["/mef-services:l2vpn"][0]["site"][0]){
            oldSites=oldInstance["/mef-services:l2vpn"][0]["site"];
        }else{
            oldSites=[]
            oldSites.push(oldInstance["/mef-services:l2vpn"][0]["site"]);
        }

        var newSite = newInstance;
        console.log(newSite);
        console.log(newSite["site-name"]);

        for(i=0;i<oldSites.length;i++){
            if(newSite["site-name"]===oldSites[i]["site-name"]){
                if(newSite["design-type"]==="PE-UNI"){
                    if(oldSites[i]["pe-contents"]["device-name"] != newSite["pe-contents"][0]["device-name"] 
                    || oldSites[i]["pe-contents"]["interface-type"] != newSite["pe-contents"][0]["interface-type"]
                    || oldSites[i]["pe-contents"]["interface-id"] != newSite["pe-contents"][0]["interface-id"]){
                        console.log("1");
                        return callback("true");
                    }else{
                        return callback("false");
                    }
                }else{
                    if(oldSites[i]["pe-contents"]["device-name"] != newSite["pe-contents"][0]["device-name"] 
                    || oldSites[i]["pe-contents"]["interface-type"] != newSite["pe-contents"][0]["interface-type"]
                    || oldSites[i]["pe-contents"]["interface-id"] != newSite["pe-contents"][0]["interface-id"]
                    || oldSites[i]["pe-contents"]["service-id-number"] != newSite["pe-contents"][0]["service-id-number"]
                    || oldSites[i]["pe-contents"]["dot1q-Id"] != newSite["pe-contents"][0]["dot1q-Id"]){
                        console.log("2");
                        return callback("true");
                    }else{
                        if(oldSites[i]["pe-contents"]["seconddot1q-Id"] != newSite["pe-contents"][0]["seconddot1q-Id"]){
                            console.log("3");
                            return callback("true");
                        }else if(oldSites[i]["pe-contents"]["dot1ad"] != newSite["pe-contents"][0]["dot1ad"]){
                            console.log("4");
                            return callback("true");
                        }else{
                            return callback("false");
                        }
                    }
                }
            }
        }
        return callback("true");
    }else if(serviceModel==="l2vpn-cpe"){ // Check CPE on L2VPN (consume-existing-uni=no) changes
        
        var oldSites=oldInstance["/mef-services:l2vpn"][0]["site"];
        var newSite = newInstance;

        for(i=0;i<oldSites.length;i++){
            if(newSite["site-name"]===oldSites[i]["site-name"]){

                if(oldSites[i]["consume-existing-uni"] != newSite["consume-existing-uni"]){
                    return callback("true");
                }else{
                    if(oldSites[i]["setup-uni-contents"]["device-name"] != newSite["setup-uni-contents"][0]["device-name"] 
                    || oldSites[i]["setup-uni-contents"]["interface-type"] != newSite["setup-uni-contents"][0]["interface-type"]
                    || oldSites[i]["setup-uni-contents"]["interface-id"] != newSite["setup-uni-contents"][0]["interface-id"]
                    || oldSites[i]["setup-uni-contents"]["circuit-id"] != newSite["setup-uni-contents"][0]["circuit-id"]){
                        return callback("true");
                    }else{
                        return callback("false");
                    }
                }
            }       
        }
        return callback("true");
    }
}

// Get a list of all the devices in a payload
// Input: payloadObject the job instance
// Output: listOfDevicesNoDuplicates an array of all the devices in the payload
CCI.prototype.payloadDevices = function(payloadObject, callback){

    // List of all devices that will be returned by the function
    var listOfDevices = []

    if(payloadObject.hasOwnProperty("/mef-services:uni")){ // Handle UNI
        listOfDevices.push(payloadObject["/mef-services:uni"][0]["device-name"]);

        // Clean the array from duplicated devices
        listOfDevices = listOfDevices.filter( function( item, index, inputArray ) {
            return inputArray.indexOf(item) == index;
        });

        // Return list of all the devices in the payload
        return callback(listOfDevices);
    }else if(payloadObject.hasOwnProperty("/mef-services:dia")){ // Handle DIA
        console.log("dia");
        console.log(payloadObject["/mef-services:dia"][0]["device-name"]);
        listOfDevices.push(payloadObject["/mef-services:dia"][0]["device-name"])

        if(payloadObject["/mef-services:dia"][0]["setup-uni-contents"]){ // Handle DIA if there is a UNI too (consume-existing-uni: no) 
            // console.log(payloadObject["/mef-services:dia"][0]["setup-uni-contents"]["device-name"]);            
            // console.log(payloadObject["/mef-services:dia"][0]["setup-uni-contents"][0]["device-name"]);
            console.log(listOfDevices);
            if(payloadObject["/mef-services:dia"][0]["setup-uni-contents"][0]){
                console.log("array");
                listOfDevices.push(payloadObject["/mef-services:dia"][0]["setup-uni-contents"][0]["device-name"])
            }else{
                console.log("not an array");
                listOfDevices.push(payloadObject["/mef-services:dia"][0]["setup-uni-contents"]["device-name"])
            }
            console.log(listOfDevices);
            
            // Clean the array from duplicated devices
            listOfDevices = listOfDevices.filter( function( item, index, inputArray ) {
                return inputArray.indexOf(item) == index;
            });

            // Return list of all the devices in the payload
            return callback(listOfDevices);
        }else{
            consumeUNI="true";
            var uniId=payloadObject["/mef-services:dia"][0]["uni"];
            var deviceId="";

            var getInstancePromise = new promise(function(resolve, reject){
                cogs.ServiceCog.getInstance("/mef-services:uni",uniId, (job, serror) => {
                    if(serror){
                        reject(serror);
                    }
                    deviceId=job["/mef-services:uni"]["device-name"];
                    if(!deviceId){
                        return callback("false");
                    }
                    resolve(deviceId);
                    // listOfDevices.push(deviceId);
                });
            });
        
            // If the job started continue
            getInstancePromise.then(function(fromResolve){
                listOfDevices.push(fromResolve);
                
                // Clean the array from duplicated devices
                listOfDevices = listOfDevices.filter( function( item, index, inputArray ) {
                    return inputArray.indexOf(item) == index;
                });

                // Return list of all the devices in the payload
                return callback(listOfDevices);

            }).catch(function(fromReject){
                // Clean the array from duplicated devices
                listOfDevices = listOfDevices.filter( function( item, index, inputArray ) {
                    return inputArray.indexOf(item) == index;
                });

                // Return list of all the devices in the payload
                return callback(listOfDevices);
            });
        }
    }else if(payloadObject.hasOwnProperty("/mef-services:l2vpn")){ // Handle L2VPN
        
        // Split each site by its own
        var siteArray = payloadObject['/mef-services:l2vpn'][0].site;
        var promisesArray=[]

        // Handle each site
        for(var i=0;i<siteArray.length;i++){
            if(siteArray[i]["design-type"]==="PE-UNI"){
                console.log("No need for device");
            }else{
                if(siteArray[i]["pe-contents"][0]){
                    listOfDevices.push(siteArray[i]["pe-contents"][0]["device-name"]);
                }else{
                    listOfDevices.push(siteArray[i]["pe-contents"]["device-name"]);
                }
            }

            if(siteArray[i]["setup-uni-contents"]){ // Handle if each site have a UNI too (consume-uni: no)
                if(siteArray[i]["setup-uni-contents"][0]){
                    listOfDevices.push(siteArray[i]["setup-uni-contents"][0]["device-name"]);
                }else{
                    listOfDevices.push(siteArray[i]["setup-uni-contents"]["device-name"]);
                }
            }else{
                var uniId=siteArray[i]["uni"];
                var deviceId="";

                promisesArray.push(new promise(function(resolve, reject){
                    cogs.ServiceCog.getInstance("/mef-services:uni",uniId, (job, serror) => {
                        if(serror){
                            reject(serror);
                        }
                        deviceId=job["/mef-services:uni"]["device-name"];
                        if(!deviceId){
                            return callback("false");
                        }
                        resolve(deviceId);
                    });
                }))
            }
        }

        if(promisesArray.length>0){
            promise.all(promisesArray).then(function(values){

                for(v=0;v<values.length;v++){
                    listOfDevices.push(values[v]);
                }

                // Clean the array from duplicated devices
                listOfDevices = listOfDevices.filter(function(item, index, inputArray) {
                    return inputArray.indexOf(item) == index;
                });
                console.log("1"+listOfDevices);

                // Return list of all the devices in the payload
                return callback(listOfDevices);
                        
                }).catch(function(){
                // Clean the array from duplicated devices
                listOfDevices = listOfDevices.filter( function( item, index, inputArray ) {
                    return inputArray.indexOf(item) == index;
                });

                // Return list of all the devices in the payload
                return callback(listOfDevices);
            })
        }else{
            // Clean the array from duplicated devices
            listOfDevices = listOfDevices.filter( function( item, index, inputArray ) {
                return inputArray.indexOf(item) == index;
            });

            // Return list of all the devices in the payload
            return callback(listOfDevices);
        }
    }
}

// Get the name for which PreCheck MOP to run for L2VPN and DIA PE
// Input: sitePayload for L2VPN this is the site, for DIA this is the entire paylaod
// Input: incomingDeviceType device type
// Output: the name of the PreCheck MOP that should be run
CCI.prototype.getMOPNameL2VPN = function(sitePaylaod, incomingDeviceType, callback){
    var designType="";
    var deviceType=incomingDeviceType;

    if(sitePaylaod["/mef-services:dia"]){ // Get the PreCheck MOP name to run for DIA PE (always IOSXR)
        designType=sitePaylaod["/mef-services:dia"][0]["design-type"]

        if(designType==="DarkFiber"){ // Check if the design type is DarkFiber then there is only dot1q
            return callback("DIA-DarkFiber-IOSXR");
        }else{ // Check if the design type is type 2 nni then there is dot1q plus either dot1ad or seconddot1q
            if(sitePaylaod["/mef-services:dia"][0]["encapsulation-type"][0]["dot1ad"]){ // Check if it contains dot1ad
                return callback("DIA-T2-IOSXR-DOT1AD");
            }else if(sitePaylaod["/mef-services:dia"][0]["encapsulation-type"][0]["seconddot1q-Id"]){ // Check if it contains seconddot1q
                return callback("DIA-T2-IOSXR-SECONDDOT1Q");
            }
        }
    }else{ // Get the PreCheck MOP name for L2VPN PE (IOS or IOSXR)
        designType=sitePaylaod["design-type"];

        if(designType==="DarkFiber"){ // Design type is darkfiber then there is only dot1q and it is either IOS or IOSXR
            if(deviceType==="cisco-ios"){
                return callback("L2VPN-DarkFiber-IOS");
            }else{
                return callback("L2VPN-DarkFiber-IOSXR");
            }
        }else if(designType==="PE-UNI"){ // Design type is PE UNI then treat it as a UNI service
            if(deviceType==="cisco-ios"){
                return callback("UNI-DIA-L2VPN-Create-IOS");
            }else{
                return callback("UNI-DIA-L2VPN-Create-IOSXR");
            }
        }else{ // Design type is type 2 nni then there is dot1q plus either dot1ad or seconddot1q or neither
            if(deviceType==="cisco-ios"){ // IOS devices
                if(sitePaylaod["pe-contents"][0]["dot1ad"]){ // dot1ad
                    return callback("L2VPN-T2-IOS-AD");
                }else if(sitePaylaod["pe-contents"][0]["seconddot1q-Id"]){ // seconddot1q
                    return callback("L2VPN-T2-IOS-2nd");
                }else{
                    return callback("L2VPN-DarkFiber-IOS"); // only seconddot1q
                }
            }else{ // IOSXR devices
                if(sitePaylaod["pe-contents"][0]["dot1ad"]){ //dot1ad
                    return callback("L2VPN-T2-IOSXR-AD");
                }else if(sitePaylaod["pe-contents"][0]["seconddot1q-Id"]){ // seconddot1q
                    return callback("L2VPN-T2-IOSXR-2nd");
                }else{
                    return callback("L2VPN-DarkFiber-IOSXR"); // only dot1q
                }
            }
    
        }
    }
}

// Combine multiple MOPS results into one MOP result
// Input: mops is an array of of all the runCommandTemplate
// Output: returnMOP is the final combined runCommandTemplate
CCI.prototype.combineMOPResults = function(mops, callback){
    var commandsResultsArray=[]; // Combine all the commands_results from the different runCommandTemplates outputs
    var mopResults=true; // This is the results of the MOP

    if(mops.length>1){ // Handle if the array of mops passed contain more than one MOP
        // For all the MOPs add commands_results to the commandsResultsArray
        var k=0;
        for(i=0;i<mops.length;i++){
            currentArray=mops[i]["commands_results"]
            for(j=0;j<currentArray.length;j++){
                commandsResultsArray[k]=currentArray[j];
                k=k+1;
            }

            // Check if one of the MOPs results is false, then set the result in the returnMOP to false.
            var r = mops[i]["result"];
            if(r == false){
                mopResults = false;
            }
        }

    // Set the skelton of the MOP, combine the results of all other MOPs into one, and set the result of the returnMOP
    var returnMOP = mops[0];
    returnMOP["commands_results"]=commandsResultsArray;
    returnMOP["result"]=mopResults;
    
    // Return the final combined MOP
    return callback(returnMOP);

    }else if(mops.length===1){ // If there is only one MOP return that one as it is
        return callback(mops[0]);
    }else{ // If there are no MOPs return false
        return callback({"results":"false"});
    }
}

// This method is needs to be used only on adapter-nso 3.8.0
// Fix to show the dryrun for deleteServicePathsDruRun workflow task
// Input: incomingDeleteDryRun the output of deleteServicePathsDruRun workflow task
// Output: modified format deleteServicePathsDruRun workflow task output
CCI.prototype.deleteDryRunFix = function(incomingDeleteDryRun, callback){
    
    // Restructure the object and return it
    var returnObj={
        "device":{}
    };

    for(var i=0;i<incomingDeleteDryRun.length;i++){
        returnObj["device"][i] = {
            "name": incomingDeleteDryRun[i]["device"],
            "data": incomingDeleteDryRun[i]["config"]
        }
    }

    return callback(returnObj);
}

// Fix to convert the payload that is coming from getInstance task
// Input: incomingPyaload the output of getInstance workflow task which comes the first key as an object
// Output: modified format for getInstance to make the first key as an array instead of an object
CCI.prototype.fixGetInstanceOutput = function(incomingPayload, callback){
    var outgoingPayload={};

    if(incomingPayload["/mef-services:uni"]){ // UNI service
        outgoingPayload["/mef-services:uni"]=[]
        outgoingPayload["/mef-services:uni"].push(incomingPayload["/mef-services:uni"]);
    }else if(incomingPayload["/mef-services:dia"]){ // DIA service
        outgoingPayload["/mef-services:dia"]=[]
        outgoingPayload["/mef-services:dia"].push(incomingPayload["/mef-services:dia"]);
    }else if(incomingPayload["/mef-services:l2vpn"]){ // L2VPN service
        outgoingPayload["/mef-services:l2vpn"]=[]
        outgoingPayload["/mef-services:l2vpn"].push(incomingPayload["/mef-services:l2vpn"]);
    }

    return callback(outgoingPayload);
}

// Check if a UNI in DIA or L2VPN need to be removed because it got provisioned in modification but the rest of the service did not
// Input: serviceType the name of the service
// Input: newPayload the new payload that is wanted to be modified
// Input: oldPayload the old payload that is currently running before modification
// Output: If there are UNIs to be deleted then we return an array of those UNIs otherwise we return false
CCI.prototype.checkUNIRollback = function(serviceType, newPayload, oldPayload, callback){
    if(serviceType==="l2vpn"){ // L2VPN service
        
        var newSites=[]; // Sites in the new payload
        var oldSites=[]; // Sites in the old payload

        var newUNIArray=[]; // The UNIs in the new payload from each site
        var oldUNIArray=[]; // The UNIs in the old payload from each site
        var uniArray=[]; // The UNIs that needs to be checked if we need to roll them back

        // Set the sites from the new payload
        if(newPayload["/mef-services:l2vpn"][0]["site"][0]){
            newSites=newPayload['/mef-services:l2vpn'][0].site;
        }else{
            newSites.push(newPayload['/mef-services:l2vpn'][0].site);
        }

        console.log("newSites: "+newSites);

        // Set the sites from the old payload
        if(oldPayload["/mef-services:l2vpn"][0]["site"][0]){
            oldSites=oldPayload['/mef-services:l2vpn'][0].site;
        }else{
            oldSites.push(oldPayload['/mef-services:l2vpn'][0].site);
        }

        console.log("oldSites: "+oldSites);

        // Find the UNIs for every site in the newSites
        for(var i=0;i<newSites.length;i++){
            console.log("newsites loop");
            if(newSites[i]["consume-existing-uni"]==="no"){
                console.log("new site have consume=no");
                if(newSites[i]["setup-uni-contents"][0]){
                    newUNIArray.push(newSites[i]["setup-uni-contents"][0]["circuit-id"]);
                }else{
                    newUNIArray.push(newSites[i]["setup-uni-contents"]["circuit-id"]);
                }
            }
        }

        console.log("newUNIArray: "+newUNIArray);
        
        // Find the UNIs for every site in the oldSites
        for(var i=0;i<oldSites.length;i++){
            if(oldSites[i]["consume-existing-uni"]==="no"){
                if(oldSites[i]["setup-uni-contents"][0]){
                    oldUNIArray.push(oldSites[i]["setup-uni-contents"][0]["circuit-id"]);
                }else{
                    oldUNIArray.push(oldSites[i]["setup-uni-contents"]["circuit-id"]);
                }
            }else{
                oldUNIArray.push(oldSites[i]["uni"]);
            }
        }

        console.log("oldUNIArray: "+oldUNIArray);

        var changed="false";

        var count=0;
        var count2=0;

        // Find the UNIs in the newUNIs but not in the oldUNIs
        for(var i=0;i<newUNIArray.length;i++){
            count=0;
            for(var j=0;j<oldUNIArray.length;j++){
                if(newUNIArray[i]===oldUNIArray[j]){
                count++;
                }
            }
            if(count<1){
                uniArray[count2]=newUNIArray[i];
                count2++;
            }
        }

        console.log("uniArray: "+uniArray);

        // Check for any UNIs in uniArray (UNIs in newUNIs but not in oldUNIs)
        if (uniArray.length>=1){
            return callback(uniArray);
        }else{
            return callback("false");
        }

    }else if(serviceType==="dia"){ // DIA service
        var newUNI=""
        var oldUNI=""

        // Get the circuitId for the UNI from the new payload
        if(newPayload["/mef-services:dia"][0]["setup-uni-contents"][0]){
            newUNI = newPayload["/mef-services:dia"][0]["setup-uni-contents"][0]["circuit-id"];
        }else{
            newUNI = newPayload["/mef-services:dia"][0]["setup-uni-contents"]["circuit-id"];
        }

        // Get the circuitId for the UNI from the old payload
        if(oldPayload["/mef-services:dia"][0]["consume-existing-uni"]==="yes"){ // If it was consume yes then get the UNI field
            oldUNI = oldPayload["/mef-services:dia"][0]["uni"];
        }else{ // If it was creating a UNI then get the circuitId inside UNI content
            if(oldPayload["/mef-services:dia"][0]["setup-uni-contents"][0]){
                console.log("old payload consume=no and it is an array");
                oldUNI = oldPayload["/mef-services:dia"][0]["setup-uni-contents"][0]["circuit-id"];
            }else{
                console.log("old payload consume=no and it is not an array");
                oldUNI = oldPayload["/mef-services:dia"][0]["setup-uni-contents"]["circuit-id"];
            }
        }

        // Check if the two UNIs match or if it is a new one
        if(newUNI===oldUNI){
            return callback("false");
        }else{
            var uniArray=[]
            uniArray.push(newUNI);
            return callback(uniArray);
        }
    }

    // If it some other service or it was misspelled then return false and do not remove any UNIs
    return callback("false");
}


// Check if the consume-existing-uni:yes/no needs to be changed in the payload for DIA and L2VPN
// Input: servicePayload the payload for DIA or L2VPN
// Output: fixed payload that might have modified if the uni exists, then consume-exsiting-uni switched from no to yes and the uni number have been added
CCI.prototype.checkUNIExists = function(servicePayload, callback){

    if(servicePayload["/mef-services:l2vpn"]){ // Handle L2VPN

        // Get an array of all the sites in the l2vpn payload
        var sitesArray = servicePayload["/mef-services:l2vpn"][0]["site"]

        // List of all the UNIs
        var UNIsArray = []

        // List of the sites fixed
        var sitesArrayFixed = []

        // Get all provisioned unis from the system
        var getUNIsPromise = new promise(function(resolve, reject){
            cogs.ServiceCog.getInstancesOfService("/mef-services:uni", (job, serror) => {
                if(serror){
                    reject(serror);
                }
                resolve(job);
            });
        });
    
        // If the get have been resolved successfully
        getUNIsPromise.then(function(fromResolve){
            UNIsArray = fromResolve;

            // If there are no running unis then return the payload as it is
            if(UNIsArray.length<=0){
                return callback(servicePayload);
            }

            // For each site in l2vpn paylaod loop on them
            for(var i=0;i<sitesArray.length;i++){

                // If the site have consume-existing-uni:yes then do not change anything and push it right away to the sitesArrayFixed
                if(sitesArray[i]["consume-existing-uni"]==="yes"){
                    sitesArrayFixed.push(sitesArray[i])
                }else{
                    var found="false";

                    for(var j=0;j<UNIsArray.length;j++){
                        if(UNIsArray[j]["key_value"]===sitesArray[i]["setup-uni-contents"][0]["circuit-id"]){
                            found="true";
                        }
                    }

                    if(found==="false"){
                        sitesArrayFixed.push(sitesArray[i])
                    }else{
                        var newTempSite=sitesArray[i]
                        newTempSite["consume-existing-uni"]="yes"
                        newTempSite["uni"]=sitesArray[i]["setup-uni-contents"][0]["circuit-id"]
                        delete newTempSite["setup-uni-contents"];
                        console.log(newTempSite);
                        sitesArrayFixed.push(newTempSite);
                    }
                }
            }

            servicePayload["/mef-services:l2vpn"][0]["site"] = sitesArrayFixed;
            return callback(servicePayload);
        }).catch(function(fromReject){
            results.status = "Failed";
            return callback(results);
        });
    }else if(servicePayload["/mef-services:dia"]){ // Handle DIA
        console.log("DIA");
        
        // An array for all the UNIs
        var UNIsArray = []

        // Get a list of all the running UNIs in the system
        var getUNIsPromise = new promise(function(resolve, reject){
            cogs.ServiceCog.getInstancesOfService("/mef-services:uni", (job, serror) => {
                if(serror){
                    reject(serror);
                }
                resolve(job);
            });
        });
    
        // If the UNIs have been found
        getUNIsPromise.then(function(fromResolve){
            UNIsArray = fromResolve;

            // If there are no provisions UNIs then just return the payload as it is
            if(UNIsArray.length<=0){
                return callback(servicePayload);
            }

            // If the payload have consume-existing-uni=yes then just return the same payload, there is nothing to modify
            if(servicePayload["/mef-services:dia"][0]["consume-existing-uni"]==="yes"){
                return callback(servicePayload);
            }else{ 
                // If the consume-existing-uni=no, then we check if we need to modify the payload and send it back
                console.log("consume-uni=no")
                var found="false";

                // Compare the uni in the payload to all the unis in the array of provisioned unis
                for(var j=0;j<UNIsArray.length;j++){
                    if(UNIsArray[j]["key_value"]===servicePayload["/mef-services:dia"][0]["setup-uni-contents"][0]["circuit-id"]){
                        found="true";
                    }
                }

                console.log("found" + found);

                // If the uni is not provisioned then return the payload as it is
                if(found==="false"){
                    return callback(servicePayload);
                }else{
                    // If the uni is found then change the payload from consume-existing-un:no to consume-existing-uni:yes and add the uni number
                    var newTempPayload = servicePayload;
                    console.log(newTempPayload);
                    console.log("I'm chaning the payload now");
                    newTempPayload["/mef-services:dia"][0]["consume-existing-uni"]="yes";
                    newTempPayload["/mef-services:dia"][0]["uni"]=servicePayload["/mef-services:dia"][0]["setup-uni-contents"][0]["circuit-id"];
                    delete newTempPayload["/mef-services:dia"][0]["setup-uni-contents"];
                    console.log(newTempPayload);
                    return callback(newTempPayload);
                }
            }
            
        }).catch(function(fromReject){
            // If there was an error in getting all the UNIs
            results.status = "Failed";
            return callback(results);
        });

    }else{ // If the payload is UNI or not correct
        return callback("false");
    }

}

module.exports = new CCI();